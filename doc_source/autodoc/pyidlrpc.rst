
***************************
 pyidlrpc public interface
***************************

This shows the documentaiton for the pyidlrpc public interface.

.. automodule:: pyidlrpc
    :members: execute 
              ,ex
              ,setVariable
              ,set
              ,getVariable
              ,get
              ,callFunction
              ,callPro
    :undoc-members:
    :show-inheritance:

.. autoclass:: PyIDL
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: PyIDLObject
    :members:
    :undoc-members:
    :show-inheritance:
