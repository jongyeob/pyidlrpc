.. pyidlrpc documentation master file, created by
   sphinx-quickstart on Tue Dec 17 23:27:06 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _pyidlrpc:

.. include:: ../README

Documentation Index
===================

.. toctree::
   :maxdepth: 2

   introduction
   autodoc/pyidlrpc
   development
   license



Indices and Tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

