# idlmath.py

import pyidlrpc as idl

def stddev(*args, **kwargs):
    return idl.callFunction('STDDEV', args, kwargs)

def mean(*args, **kwargs):
    return idl.callFunction('MEAN', args, kwargs)
