# idlmath.py

import pyidlrpc as idl

def stddev(input):
    return idl.callFunction('STDDEV', [input])

def mean(input):
    return idl.callFunction('MEAN', [input])
